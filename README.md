# OCP Maven Example Builds

## Binary Build From Local

This section contains the steps for completing a binary build on your local environment.

Create a new project on your openshift cluster

```
oc new-project maven-binary-build
```

create a build config, specifying a docker strategy using binary rules, and the container image that your application will be hosted on.

```
oc new-build --strategy docker --binary --docker-image centos:centos7 --name maven-binary-build-app
```

To start a binary build, you must specify a source from which to build from. In this samples case, the container runs the java application jar and outputs the results into a file, that file is then tailed in the container. If the Jar is missing, the build will fail. This sample was created to show that you can customize how the application is build before it is built into a container image. 

To build the application locally:

```
mvn clean package
```

To start the build:

```
oc start-build maven-binary-build-app --from-dir . --follow
```

Once the image has been build, you can create a deployment for the app:

```
oc new-app maven-binary-build-app
```

## Docker build

This section contains the steps for completing a binary build using this sample app. A branch has been created that has a separate docker file, with the instructions to build and package the application.

```
oc new-app --name maven-app-docker https://gitlab.com/cawright/maven-binary-build.git#docker-build
```


