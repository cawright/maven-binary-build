FROM centos:centos7

COPY . /opt/myapp

RUN cd /opt \
  && yum -y install java-1.8.0-openjdk \
  && yum clean all 

RUN ls /opt/myapp

EXPOSE 8080

RUN cd /opt/myapp \
  && java -cp target/my-app-1.0-SNAPSHOT.jar com.mycompany.app.App > appoutput.txt

WORKDIR /opt/myapp

CMD ["tail", "-f", "appoutput.txt"]
